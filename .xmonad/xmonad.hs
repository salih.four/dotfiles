import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys, removeKeys)
import System.IO
import System.Exit

winM = mod4Mask
altM = mod1Mask

main = do
    xmproc <- spawnPipe "xmobar"

    xmonad $ docks defaultConfig
        { manageHook = manageDocks <+> manageHook defaultConfig
        , layoutHook = avoidStruts  $  layoutHook defaultConfig
        , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
                        }
        , modMask = mod4Mask     -- Rebind Mod to the Windows key
        , terminal = "konsole"
        } `removeKeys`
        [ (mod4Mask .|. shiftMask, xK_q)
        , (mod4Mask .|. shiftMask, xK_c)
        , (mod4Mask, xK_p)
        ] `additionalKeys`
        [ ((winM .|. shiftMask, xK_z), spawn "i3lock-fancy; xscreensaver-command -lock; xset dpms force off")
        , ((winM .|. shiftMask, xK_x), spawn "i3lock-fancy; systemctl suspend")
        , ((winM .|. altM, xK_p), spawn "mypomodoro start")
        , ((winM .|. shiftMask, xK_q), kill)
        , ((winM, xK_p), spawn "rofi -show run")
        , ((winM, xK_s), spawn "sleep 0.2; flameshot gui")
        , ((altM, xK_g), spawn "glxgears")
        , ((winM .|. shiftMask, xK_e), io (exitWith ExitSuccess)) 
        , ((winM .|. altM, xK_u), spawn "brightnessctl --device=radeon_bl0 set 10%+")
        , ((winM .|. altM, xK_d), spawn "brightnessctl --device=radeon_bl0 set 10%-")
        , ((winM .|. altM, xK_r), spawn "pulseaudio-ctl up")
        , ((winM .|. altM, xK_l), spawn "pulseaudio-ctl down")
        ]

