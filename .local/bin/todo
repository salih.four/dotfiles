#!/bin/bash

# Note
# ====
# Required packages: vim, fzf and exa
# Technically, exa could be substituted with ls but its a personal choice.
# Maybe I'll rewrite it to use ls for compatability

#----------------------------------------

# Configure your todo directory here
tododir="$HOME/todos"
editor=nvim

# ---------------------------------------

[ ! -d "$tododir" ] && mkdir "$tododir"
[ ! -d "$tododir/done" ] && mkdir "$tododir/done"

_usage(){
  cat << EOF
  Usage: $(basename "$0") [option]

    Options
    -------
    add              -   add a todo
    list             -   list all todos, edit them and mark them done. Same as calling with no arguments
    listdone         -   list all done todos and mark them undone
    search <query>   -   search all todos for the query
    date             -   edit the todo for the current date [Zettelskaten]
    later            -   aggregate all "chack laters" from dated todos
EOF
}
_add(){
    if [ -n "$1" ];then
        title="$1"
    else
        printf 'Add todo\n❯ '
        read -r title
    fi
  if [ -n "$title" ]; then
    ftitle="${title// /_}"
    todofile="$tododir/$ftitle.md"
    if [ ! -f "$title" ]; then
        printf "# %s\n" "$title" > "$todofile"
        $editor +'normal Go' "$todofile"
    fi
  fi
}
_list(){
    # c-space:  mark as done
    # c-d:      show done: goto next state of _list_done
    # c-a:      add todo by spawning new terminal: doesnt update list after done: so need to reload manually with c-r
    # c-r:      reload list [typically after adding which doesnt have callback]                              
    FZF_DEFAULT_COMMAND="$0 ls_internal" 
    editfile=$(FZF_DEFAULT_COMMAND="$FZF_DEFAULT_COMMAND" fzf -0 \
                --header="Your Todos"\
                --color=16 \
                --header="Ctrl-Space: mark as done | Ctrl-d: show done" \
                --bind "ctrl-space:execute-silent[$0 done {}]+reload($FZF_DEFAULT_COMMAND)" \
                --bind "ctrl-d:execute($0 listdone)+reload($FZF_DEFAULT_COMMAND)" \
                --bind "ctrl-a:execute($TERMINAL -e $0 add)+reload($FZF_DEFAULT_COMMAND)" \
                --bind "ctrl-r:reload($FZF_DEFAULT_COMMAND)" \
                --bind "ctrl-s:execute[$0 add]" \
                --cycle \
                --border=horizontal \
                --height 50% \
                --preview 'bat --color=always {} 2>/dev/null')
                #--bind "ctrl-d:reload($FZF_DEFAULT_COMMAND d)" \
    [ $? -eq 1 ] && echo -e "Couldnt find any todos\nAdd a todo to access menu"
    [ -f "$editfile" ] && $editor "$editfile"
}
_list_done(){
    # c-space:  mark not done
    # c-t:      show todo: goto previous state _list
    # del:      delete a todo
    FZF_DEFAULT_COMMAND="$0 ls_internal d" 
    editfile=$(FZF_DEFAULT_COMMAND="$FZF_DEFAULT_COMMAND" fzf \
                --header="Ctrl-Space: mark not done | Ctrl-t: show pending" \
                --bind "ctrl-space:execute-silent[$0 done {}]+reload($FZF_DEFAULT_COMMAND)" \
                --bind "ctrl-t:abort" \
                --bind "del:execute-silent[$0 delete {}]+reload($FZF_DEFAULT_COMMAND)" \
                --cycle \
                --border=horizontal \
                --height 50% \
                --preview 'bat --color=always {} 2>/dev/null')
    [ $? -eq 1 ] && echo "Couldnt find any todos"
    # Dont call vim because:
    # a) this isnt a terminal so it wont work
    # b) done todos should not be edited. So a feature? :)
}
_mark_done(){
    basetodopath=$(basename "$tododir")
    filepath="$1"
    filedir=$(dirname "$filepath")
    todostatus=$(basename "$filedir")
    filename=$(basename "$filepath")
    [ "$todostatus" = "done" ] && mv "$tododir/done/$filename" "$tododir/$filename"
    [ "$todostatus" = "$basetodopath" ] && mv "$tododir/$filename" "$tododir/done/$filename"
}

if [ $# -lt 1 ]; then
    #_usage
    #exit 1
    clear
    _list
    exit 0
fi
if [ "$1" = "add" ]; then
    clear
    _add "$2"
elif [ "$1" = "list" ];then
    clear
    _list
elif [ "$1" = "listdone" ];then
    clear
    _list_done
elif [ "$1" = "done" ];then
    clear
    _mark_done "$2"
elif [ "$1" = "delete" ];then
    rm "$2"
elif [ "$1" = "search" ];then
    [ -z "$2" ] && echo "No search query given" && exit 1
    rg -S "$2" "$tododir"
elif [ "$1" = "date" ];then
    datemd="$(date -Idate).md"
    filep="$tododir/dated/$datemd"
    if [ ! -f "$filep" ]; then
        echo "creating"
        cp "$tododir/dated/.template.md" "$filep"
        prevfile="$tododir/dated/$(date --date='1 day ago' -Idate).md"
        if [ -f "$prevfile" ]; then
            echo "copying unfinished todo"
            carried="$(sed -n -e '/# DO/,/^---$/p' "$prevfile" | head -n -1 | tail -n +2 | sed -e '/^$/d')"
            sed -e '/# DO/r'<(echo "$carried") -i "$filep"
        fi
    fi
    $editor "$filep"
elif [ "$1" = "later" ];then
    echo -e "# Check Later\n"
    for i in $(/bin/ls "$tododir/dated");do 
     echo "## ${i%.md}"
     sed -n -e '/Check\ Later/,$p' "$tododir/dated/$i" | tail -n +2
    done
elif [ "$1" = "pdfdated" ];then
    echo -e "# Dated\n"
    for i in $(env ls "$tododir/dated");do 
     echo -e "\n# ${i%.md}\n"
     sed 's/^#/##/' "$tododir/dated/$i"
    done
elif [ "$1" = "ls_internal" ];then
    dir=""
    [ "$2" = "d" ] && dir="$tododir/done" || dir="$tododir"
    list=$(exa -rs modified "$dir"/*.md)
    unset dir
    echo "$list"
else
  echo "Invalid argument $2"
  _usage
  exit 1
fi
