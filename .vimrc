set number
let tablength=4
let &tabstop = tablength 
let &shiftwidth = tablength 
set expandtab 
set smarttab 
set smartindent
set colorcolumn=81
nnoremap <c-o> :w<cr>
nnoremap <c-x> :q<cr>


" Auto install vim plugin manager vim-plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Vim Plug section
call plug#begin('~/.vim/vim_plugged')
Plug 'airblade/vim-rooter'
Plug 'ryanoasis/vim-devicons'
Plug 'preservim/nerdtree'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'tpope/vim-commentary'
Plug 'preservim/nerdcommenter'
Plug 'ycm-core/YouCompleteMe'
Plug 'junegunn/fzf.vim'
Plug 'metakirby5/codi.vim'
Plug 'mattn/emmet-vim'
Plug 'evanleck/vim-svelte'
Plug 'tpope/vim-fugitive'
call plug#end()
filetype plugin indent on
syntax on

" NERDTree shortcut
nnoremap <c-t> :NERDTreeToggle<cr>
let NERDTreeShowHidden = 1
" Tree upon opening directory
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif
autocmd! bufwritepost .vimrc source %
" For more nerdtree defaults refer https://github.com/preservim/nerdtree

" Highlight and move up / down
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
